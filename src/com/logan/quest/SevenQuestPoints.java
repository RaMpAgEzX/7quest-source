package com.logan.quest;


import com.logan.quest.quest.restless_ghost.*;
import com.logan.quest.quest.romeo_and_juliet.*;
import com.logan.quest.quest.sheep_shearer.EndingSheep;
import com.logan.quest.quest.sheep_shearer.ObtainWool;
import com.logan.quest.quest.sheep_shearer.StartSheepShearer;
import org.rspeer.runetek.api.Varps;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.ScriptCategory;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.task.TaskScript;
import org.rspeer.ui.Log;

import java.awt.*;


@ScriptMeta(name = "Logan's 7QuestPoints", desc = "Obtains seven quest points", developer = "Logan", category = ScriptCategory.QUESTING)
public class SevenQuestPoints extends TaskScript implements RenderListener {

    @Override
    public void onStart() {
        if (Varps.get(144) != 100) {
            this.submit(new StartRomeoAndJuliet(), new TalkToJuliet(), new TalkToRomeo(), new TalkToFather(), new Berries(), new BerriesContinued(), new Ending(), new Checker()); //Romeo tasks
        }
        if (Varps.get(107) != 5) {
            this.submit(new StartRestlessGhost(), new FatherU(), new FreeingGhost(), new GetTheSkull(), new End(), new Checker());
        }
        if (Varps.get(179) != 21) {
            this.submit(new StartSheepShearer(), new ObtainWool(), new EndingSheep(), new Checker());
        } else {
            if ((Varps.get(144) == 100) && (Varps.get(107) == 5) && (Varps.get(179) == 21)) {
                Log.info("All quests are completed. Stopping the script.");
                setStopping(true);
            }
        }
    }

    @Override
    public void notify(RenderEvent renderEvent) {
        if (Quest.ROMEO_AND_JULIET.getVarpValue() != 100) {
            Graphics2D g = (Graphics2D) renderEvent.getSource();
            g.drawString("Quest: " + Quest.ROMEO_AND_JULIET.toString(), 30, 30);
            g.drawString("Varp Value (144): " + Quest.ROMEO_AND_JULIET.getVarpValue(), 30, 50);

        }
        if (Quest.THE_RESTLESS_GHOST.getVarpValue() != 5 && Quest.ROMEO_AND_JULIET.getVarpValue() == 100) {
            Graphics2D g = (Graphics2D) renderEvent.getSource();
            g.drawString("Quest: " + Quest.THE_RESTLESS_GHOST.toString(), 30, 30);
            g.drawString("Varp Value (107): " + Quest.THE_RESTLESS_GHOST.getVarpValue(), 30, 50);


        }
        if (Quest.SHEEP_SHEARER.getVarpValue() != 21 && Quest.THE_RESTLESS_GHOST.getVarpValue() == 5) {
            Graphics2D g = (Graphics2D) renderEvent.getSource();
            g.drawString("Quest: " + Quest.SHEEP_SHEARER.toString(), 30, 30);
            g.drawString("Varp Value (179): " + Quest.SHEEP_SHEARER.getVarpValue(), 30, 50);
        }
    }
}
