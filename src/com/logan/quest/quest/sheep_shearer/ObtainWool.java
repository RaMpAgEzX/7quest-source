package com.logan.quest.quest.sheep_shearer;

import com.logan.quest.Quest;
import org.rspeer.runetek.adapter.component.InterfaceComponent;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.Pickable;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.path.Path;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Pickables;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;

import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.function.BooleanSupplier;
import java.util.function.Predicate;

public class ObtainWool extends Task {
    private static final Predicate<String> IM_BACK_PREDICATE = o -> o.contains("I'm back!");
    private final Player local = Players.getLocal();
    private Area area = Area.rectangular(3193, 3276, 3212, 3257, 0);
    private final Predicate<Npc> SHEEP = npc -> npc.containsAction("Shear") && npc.getId() != 731;

    private static boolean sleepUntilForDuration(BooleanSupplier condition, long duration, long timeout, int threshold) {
        Objects.requireNonNull(condition, "Condition must not be null");

        long start = System.currentTimeMillis();
        Long trueTime = null;
        long now;
        do {
            now = System.currentTimeMillis();
            if (condition.getAsBoolean()) {
                if (trueTime == null) trueTime = now;
                long diff = now - trueTime;
                System.out.println("Diff: " + diff);
                if (diff > duration) {
                    return true;
                }
            } else {
                System.out.println("False eval");
                trueTime = null;
            }

            Time.sleep(threshold);
        } while (now - start < timeout);
        return false;
    }

    private void bank() {
        if (Bank.isOpen()) {
            Bank.depositInventory();
        } else {
            final SceneObject Bank_Booth = SceneObjects.getNearest("Bank booth");
            if (Bank_Booth != null) {
                Bank_Booth.interact(a -> true);
            }
        }
    }

    @Override
    public boolean validate() {
        return Quest.SHEEP_SHEARER.getVarpValue() == 1;
    }

    @Override
    public int execute() {
        Pickable Shears = Pickables.getNearest("Shears");
        if (Movement.getRunEnergy() > 20 && !Movement.isRunEnabled()) { //Turn on run if it's off with over 20 energy
            Movement.toggleRun(true);

        }
        int freeInvent = 28 - Inventory.getCount();
        if (freeInvent < 20 && (!Inventory.contains("Wool")) && (!Inventory.contains("Ball of Wool")) && Inventory.getCount() != 0) {
            final SceneObject Bank_Booth = SceneObjects.getNearest("Bank booth");
            if (Bank_Booth != null) {
                bank();
            } else {
                Movement.buildPath(BankLocation.getNearest().getPosition()).walk();
            }
            return 300;
        }
        if (Inventory.contains(1735)) {
            if (Inventory.getCount("Ball of Wool") >= 20) {
                int backLength = Interfaces.filterByText(IM_BACK_PREDICATE).length;
                final Npc Fred = Npcs.getNearest(732);
                if (Fred != null) {
                    if (Dialog.canContinue()) {
                        if (Dialog.processContinue()) {
                            Time.sleep(1000);
                        }
                    } else {
                        if (backLength > 0) {
                            if (Dialog.process(0)) {
                                Time.sleep(500);
                            }
                        } else {
                            if (backLength == 0) {
                                Movement.buildPath(Fred).walk();
                                Time.sleepUntil((() -> Fred.distance(local) < 4), 300, 4000);
                                Fred.interact("Talk-to");
                                return 300;
                            }
                        }
                    }
                } else {
                    Movement.buildPath(new Position(3189, 3275, 0)).walk();
                }
                return 300;
            }
            if (Inventory.getCount(1737) >= 20) {
                final SceneObject spinningWheel = SceneObjects.getNearest(14889);
                if (spinningWheel != null) {
                    InterfaceComponent Box = Interfaces.getComponent(270, 14);
                    if ((Box != null)) {
                        if (Box.isVisible()) {
                            Box.click();
                            return 300;
                        }
                    }
                    if (spinningWheel.distance(local) < 6) {
                        sleepUntilForDuration(() -> (local.getAnimation() == -1), 1500, TimeUnit.SECONDS.toMillis(60), 50);
                        spinningWheel.interact("Spin");
                    } else {

                        Movement.buildPath(spinningWheel.getPosition()).walk();
                    }
                } else {
                    Path bankPath = Movement.buildPath(new Position(3205, 3219, 1));
                    if (!area.contains(local)) {
                        bankPath.walk();
                    } else {
                        Movement.walkTo(new Position(3208, 3252, 0));
                    }
                }
            } else {
                if (SHEEP != null && area.contains(Npcs.getNearest(SHEEP)) && area.contains(local)) {
                    Npcs.getNearest(SHEEP).interact("Shear");
                } else {
                    if (!area.contains(local)) {
                        Movement.buildPath(area.getCenter().getPosition()).walk();
                    }
                }
            }
            return 300;
        } else {
            if (Shears != null) {
                Movement.buildPath(Shears).walk();
                Time.sleepUntil((() -> Shears.distance(local) < 4), 300, 1500);
                Shears.interact("Take");
            } else {
                Movement.buildPath(new Position(3189, 3275, 0)).walk();
            }
        }
        return 300;
    }
}


