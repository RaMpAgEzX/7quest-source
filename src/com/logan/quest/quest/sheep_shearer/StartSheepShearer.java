package com.logan.quest.quest.sheep_shearer;

import com.logan.quest.Quest;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;

import java.util.function.Predicate;


public class StartSheepShearer extends Task {
    private static final Predicate<String> IM_LOOKING_FOR_PREDICATE = o -> o.contains("looking for a quest");
    private static final Predicate<String> YES_OKAY_PREDICATE = o -> o.contains("Yes okay. I can do");

    private final Player local = Players.getLocal();

    private void bank() {
        if (Bank.isOpen()) {
            Bank.depositInventory();
        } else {
            final SceneObject Bank_Booth = SceneObjects.getNearest("Bank booth");
            if (Bank_Booth != null) {
                Bank_Booth.interact(a -> true);
            }
        }
    }

    @Override
    public boolean validate() {
        return Quest.SHEEP_SHEARER.getVarpValue() == 0;
    }

    @Override
    public int execute() {
        int freeInvent = 28 - Inventory.getCount();
        if (Movement.getRunEnergy() > 20 && !Movement.isRunEnabled()) { //Turn on run if it's off with over 20 energy
            Movement.toggleRun(true);

        }
        if (freeInvent < 20) {
            final SceneObject Bank_Booth = SceneObjects.getNearest("Bank booth");
            if (Bank_Booth != null && local.getFloorLevel() == 2) {
                bank();
            } else {
                if (Movement.buildPath(BankLocation.LUMBRIDGE_CASTLE.getPosition()) != null) {
                    Movement.buildPath(BankLocation.LUMBRIDGE_CASTLE.getPosition()).walk();
                }
            }
            return 300;
        }
        if (Dialog.canContinue()) {
            if (Dialog.processContinue()) {
                Time.sleep(1000);
            }
        } else {
            int lookingforLength = Interfaces.filterByText(IM_LOOKING_FOR_PREDICATE).length;
            int yesokayLength = Interfaces.filterByText(YES_OKAY_PREDICATE).length;
            if (lookingforLength > 0) {
                if (Dialog.process(0)) {
                    Time.sleep(1000);
                }
            } else if (yesokayLength > 0) {
                if (Dialog.process(0)) {
                    Time.sleep(1000);
                }
            } else {
                final Npc Fred = Npcs.getNearest(732);
                if (lookingforLength == 0) {
                    if (Fred != null) {
                        Movement.buildPath(Fred).walk();
                        Time.sleepUntil((() -> Fred.distance(local) < 2), 300, 400);
                        Fred.interact("Talk-to");
                    } else {
                        Movement.buildPath(new Position(3189, 3275, 0)).walk();
                    }
                }
            }
        }
        return 300;
    }

}


