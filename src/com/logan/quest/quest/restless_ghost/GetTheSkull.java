package com.logan.quest.quest.restless_ghost;

import com.logan.quest.Quest;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;

public class GetTheSkull extends Task {

    @Override
    public boolean validate() {
        return Quest.THE_RESTLESS_GHOST.getVarpValue() == 3;
    }

    @Override
    public int execute() {
        Player local = Players.getLocal();
        if (Movement.getRunEnergy() > 20 && !Movement.isRunEnabled()) { //Turn on run if it's off with over 20 energy
            Movement.toggleRun(true);

        }
        SceneObject ladder = SceneObjects.getNearest(2147);
        if (ladder != null) {
            Movement.buildPath(ladder).walk();
            Time.sleepUntil((() -> ladder.distance(local) < 6), 300, 4000);
            ladder.interact("Climb-down");
        } else {
            SceneObject altar = SceneObjects.getNearest(2146);
            if (altar != null) {
                Movement.buildPath(altar).walk();
                Time.sleepUntil((() -> altar.distance(local) < 6), 300, 4000);
                altar.interact("Search");
            } else {
                Movement.buildPath(new Position(3105, 3160, 0)).walk();
            }

        }
        return 300;
    }
}