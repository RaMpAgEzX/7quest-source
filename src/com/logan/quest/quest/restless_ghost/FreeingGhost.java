package com.logan.quest.quest.restless_ghost;

import com.logan.quest.Quest;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;

import java.util.function.Predicate;


public class FreeingGhost extends Task {
    private static final Predicate<String> YEP_NOW_TELL_ME_PREDICATE = o -> o.contains("Yep, now tell me what");

    private final Player local = Players.getLocal();

    @Override
    public boolean validate() {
        return Quest.THE_RESTLESS_GHOST.getVarpValue() == 2;
    }

    @Override
    public int execute() {
        final Predicate<Item> Necklace = item -> item.getId() == 552;
        int tellmeLength = Interfaces.filterByText(YEP_NOW_TELL_ME_PREDICATE).length;
        if (Movement.getRunEnergy() > 20 && !Movement.isRunEnabled()) { //Turn on run if it's off with over 20 energy
            Movement.toggleRun(true);

        }
        if (Inventory.getFirst(Necklace) != null) {
            Inventory.getFirst(Necklace).interact("Wear");
        }
        if (Dialog.canContinue()) {
            if (Dialog.processContinue()) {
                Time.sleep(1000);
            }
        } else {
            if (tellmeLength > 0) {
                if (Dialog.process(0)) {
                    Time.sleep(1000);
                }
            } else {
                final Npc ghost = Npcs.getNearest(922);
                SceneObject coffinClosed = SceneObjects.getNearest(2145);
                SceneObject coffinOpen = SceneObjects.getNearest(15061);
                if (tellmeLength == 0) {
                    if (ghost != null) {
                        Movement.buildPath(ghost).walk();
                        Time.sleepUntil((() -> ghost.distance(local) < 2), 300, 1000);
                        ghost.interact("Talk-to");
                    } else {
                        if (coffinClosed != null) {
                            Movement.buildPath(coffinClosed).walk();
                            Time.sleepUntil((() -> coffinClosed.distance(local) < 2), 300, 1500);
                            coffinClosed.interact("Open");
                        } else if (coffinOpen != null) {
                            Movement.buildPath(coffinOpen).walk();
                            Time.sleepUntil((() -> coffinOpen.distance(local) < 2), 300, 1500);
                            coffinOpen.interact("Search");
                        } else {
                            Movement.buildPath(new Position(3249, 3192, 0)).walk();
                        }
                    }
                }
            }
        }
        return 300;
    }
}


