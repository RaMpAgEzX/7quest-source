package com.logan.quest.quest.restless_ghost;

import com.logan.quest.Quest;
import org.rspeer.runetek.adapter.Interactable;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.Varps;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;


public class End extends Task {
    private final Player local = Players.getLocal();

    @Override
    public boolean validate() {
        return Quest.THE_RESTLESS_GHOST.getVarpValue() == 4;

    }

    private void useItemOn(String itemName, Interactable target) {
        if (Inventory.isItemSelected()) {
            if (target.interact("Use")) {
                Time.sleepUntil(() -> Varps.get(107) != 4, 30 * 1000);
            }
        } else {
            Inventory.getFirst(itemName).interact("Use");
        }
    }

    @Override
    public int execute() {
        SceneObject ladder = SceneObjects.getFirstAt(new Position(3103, 9576, 0));
        SceneObject coffinClosed = SceneObjects.getNearest(2145);
        SceneObject coffinOpen = SceneObjects.getNearest(15061);
        if (Movement.getRunEnergy() > 20 && !Movement.isRunEnabled()) { //Turn on run if it's off with over 20 energy
            Movement.toggleRun(true);

        }
        if (Inventory.contains(553)) {
            if (ladder != null) {
                Movement.buildPath(ladder).walk();
                ladder.interact("Climb-Up");
            }
            if (coffinClosed != null) {
                Movement.buildPath(coffinClosed).walk();
                Time.sleepUntil((() -> coffinClosed.distance(local) < 2), 300, 1500);
                coffinClosed.interact("Open");
            } else if (coffinOpen != null) {
                Movement.buildPath(coffinOpen).walk();
                Time.sleepUntil((() -> coffinOpen.distance(local) < 6), 300, 1500);
                useItemOn("Ghost's Skull", coffinOpen);

            } else {
                if (ladder == null) {
                    Movement.buildPath(new Position(3249, 3192, 0)).walk();
                }
            }
        }

        return 300;
    }
}



