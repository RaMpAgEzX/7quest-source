package com.logan.quest.quest.restless_ghost;

import com.logan.quest.Quest;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

import java.util.function.Predicate;


public class FatherU extends Task {
    private static final Predicate<String> GOT_A_GHOST_PREDICATE = o -> o.contains("He's got a ghost");
    private static final Predicate<String> FATHER_AERECK_SENT_PREDICATE = o -> o.contains("Father Aereck sent me");

    private final Player local = Players.getLocal();

    @Override
    public boolean validate() {
        return Quest.THE_RESTLESS_GHOST.getVarpValue() == 1;
    }

    @Override
    public int execute() {
        int fatherLength = Interfaces.filterByText(FATHER_AERECK_SENT_PREDICATE).length;
        int gotGhostLength = Interfaces.filterByText(GOT_A_GHOST_PREDICATE).length;
        if (Movement.getRunEnergy() > 20 && !Movement.isRunEnabled()) { //Turn on run if it's off with over 20 energy
            Movement.toggleRun(true);

        }
        if (Dialog.canContinue()) {
            if (Dialog.processContinue()) {
                Time.sleep(1000);
            }
        } else {
            if (fatherLength > 0) {
                if (Dialog.process(1)) {
                    Time.sleep(1000);
                }
            } else if (gotGhostLength > 0) {
                if (Dialog.process(0)) {
                    Time.sleep(1000);
                }
            } else {
                final Npc Father_U = Npcs.getNearest(923);
                if (fatherLength == 0 && gotGhostLength == 0) {
                    if (Father_U != null) {
                        Movement.buildPath(Father_U).walk();
                        Time.sleepUntil((() -> Father_U.distance(local) < 2), 300, 1000);
                        Father_U.interact("Talk-to");
                    } else {
                        Movement.buildPath(new Position(3147, 3175, 0)).walk();
                    }
                }
            }
        }
        return 300;
    }
}


