package com.logan.quest.quest.romeo_and_juliet;

import com.logan.quest.Quest;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

import java.util.function.Predicate;

public class StartRomeoAndJuliet extends Task {
    private static final Predicate<String> HELP_DIALOGUE_PREDICATE = o -> o.contains("Perhaps I could help");
    private static final Predicate<String> LET_HER_KNOW_DIALOGUE_PREDICATE = o -> o.contains("Yes, ok, I'll");
    private final Player local = Players.getLocal();

    @Override
    public boolean validate() {
        return Quest.ROMEO_AND_JULIET.getVarpValue() == 0;
    }

    @Override
    public int execute() {
        if (Movement.getRunEnergy() > 20 && !Movement.isRunEnabled()) { //Turn on run if it's off with over 20 energy
            Movement.toggleRun(true);

        }
        if (Dialog.canContinue()) {
            if (Dialog.processContinue()) {
                Time.sleep(1000);
            }
        } else {
            if (local.distance(new Position(3213, 3428, 0)) > 20) {
                Movement.buildPath(new Position(3213, 3428, 0)).walk();
            }
            int helpLength = Interfaces.filterByText(HELP_DIALOGUE_PREDICATE).length;
            int knowLength = Interfaces.filterByText(LET_HER_KNOW_DIALOGUE_PREDICATE).length;
            if (helpLength > 0) {
                if (Dialog.process(2)) {
                    Time.sleep(1000);
                }
            } else if (knowLength > 0) {
                if (Dialog.process(0)) {
                    Time.sleep(1000);
                }
            } else {
                final Npc romeo = Npcs.getNearest("Romeo");
                if (helpLength == 0 && knowLength == 0) {
                    if (romeo != null) {
                        romeo.interact("Talk-to");
                    }
                }
            }
        }
        return 300;
    }
}
