package com.logan.quest.quest.romeo_and_juliet;

import com.logan.quest.Quest;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.path.PredefinedPath;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;

import java.util.function.Predicate;

public class Berries extends Task {
    private static final Predicate<String> TALK_ABOUT_PREDICATE = o -> o.contains("Talk about something");
    private static final Predicate<String> TALK_ABOUT_ROMEO_PREDICATE = o -> o.contains("Talk about Romeo");
    private Position[] path = {
            new Position(3268, 3372, 0),
            new Position(3278, 3373, 0),
            new Position(3288, 3374, 0),
            new Position(3291, 3375, 0),
            new Position(3291, 3385, 0),
            new Position(3291, 3387, 0),
            new Position(3290, 3397, 0),
            new Position(3289, 3406, 0),
            new Position(3284, 3415, 0),
            new Position(3284, 3415, 0),
            new Position(3278, 3423, 0),
            new Position(3278, 3424, 0),
            new Position(3271, 3429, 0),
            new Position(3261, 3428, 0),
            new Position(3259, 3428, 0),
            new Position(3249, 3428, 0),
            new Position(3244, 3428, 0),
            new Position(3234, 3429, 0),
            new Position(3232, 3429, 0),
            new Position(3222, 3428, 0),
            new Position(3220, 3428, 0),
            new Position(3213, 3421, 0),
            new Position(3210, 3419, 0),
            new Position(3210, 3409, 0),
            new Position(3210, 3405, 0),
            new Position(3202, 3406, 0),
            new Position(3199, 3400, 0),
            new Position(3191, 3399, 0),
            new Position(3189, 3402, 0),
            new Position(3195, 3403, 0)
    };
    @Override
    public boolean validate() {
        return Quest.ROMEO_AND_JULIET.getVarpValue() == 40;
    }
    @Override
    public int execute() {
        Player local = Players.getLocal();
        final Npc Apothecary = Npcs.getNearest(5036);
        SceneObject Bush = SceneObjects.getNearest("Cadava Bush");
        if (Movement.getRunEnergy() > 20 && !Movement.isRunEnabled()) { //Turn on run if it's off with over 20 energy
            Movement.toggleRun(true);

        }
        if (!Inventory.contains(753)) {
            if (Bush != null && Bush.getId() != 23627) {
                Bush.interact("Pick-from");
            } else {
                Movement.buildPath(new Position(3271, 3369, 0)).walk();
            }
        } else {
            int canyouLength = Interfaces.filterByText(TALK_ABOUT_PREDICATE).length;
            int romeoLength = Interfaces.filterByText(TALK_ABOUT_ROMEO_PREDICATE).length;
            if (Apothecary != null) {
                if (Dialog.canContinue()) {
                    if (Dialog.processContinue()) {
                        Time.sleep(500);
                    }
                } else {
                    if (canyouLength > 0) {
                        if (Dialog.process(1)) {
                            Time.sleep(500);
                        }
                    }
                    if (romeoLength > 0) {
                        if (Dialog.process(0)) {
                            Time.sleep(500);
                        }
                    } else {
                        if (canyouLength == 0 && romeoLength == 0 && !local.isMoving() && local.getAnimation() == -1) {
                            Apothecary.interact("Talk-to");
                        }

                    }
                }
            } else {
                PredefinedPath.build(path).walk();
            }
        }
        return 300;
    }
}


