package com.logan.quest.quest.romeo_and_juliet;

import com.logan.quest.Quest;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.path.Path;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;

public class TalkToJuliet extends Task {

    @Override
    public boolean validate() {
        return Quest.ROMEO_AND_JULIET.getVarpValue() == 10;
    }

    @Override
    public int execute() {
        final Player local = Players.getLocal();
        SceneObject sTAIRCASE = SceneObjects.getNearest(11797);
        final Npc Juliet = Npcs.getNearest(5035);
        if (Movement.getRunEnergy() > 20 && !Movement.isRunEnabled()) { //Turn on run if it's off with over 20 energy
            Movement.toggleRun(true);
        }
        if (sTAIRCASE != null && sTAIRCASE.distance(local) < 7) {
            sTAIRCASE.interact("Climb-up");
        } else {
            if (local.getFloorLevel() != 1) {
                Path julietPath = Movement.buildPath(new Position(3161, 3434, 0));
                if (julietPath != null) {
                    julietPath.walk();
                }
            }
            if (local.getFloorLevel() == 1) {
                if (Dialog.canContinue()) {
                    if (Dialog.processContinue()) {
                        Time.sleep(1000);
                    }
                } else {
                    if (Movement.buildPath(Juliet) != null) {
                        Movement.buildPath(Juliet).walk();
                        Time.sleepUntil((() -> Juliet.distance(local) < 1), 300, 400);
                        Npcs.getNearest(5035).interact("Talk-to");
                    }
                }
            }

        }
        return 300;
    }
}


