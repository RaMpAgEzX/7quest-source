package com.logan.quest.quest.romeo_and_juliet;

import com.logan.quest.Quest;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

public class TalkToFather extends Task {

    @Override
    public boolean validate() {
        return Quest.ROMEO_AND_JULIET.getVarpValue() == 30;
    }

    @Override
    public int execute() {
        Player local = Players.getLocal();
        final Npc Father = Npcs.getNearest(5038);
        if (Movement.getRunEnergy() > 20 && !Movement.isRunEnabled()) { //Turn on run if it's off with over 20 energy
            Movement.toggleRun(true);

        }
        if (Father != null) {
            if (Dialog.canContinue()) {
                if (Dialog.processContinue()) {
                    Time.sleep(1000);
                }
            } else {
                if (!Game.getClient().isCameraLocked()) {
                    Movement.buildPath(Father).walk();
                    Time.sleepUntil((() -> Father.distance(local) < 2), 300, 400);
                    Npcs.getNearest(5038).interact("Talk-to");
                }
            }
        } else {
            Movement.buildPath(new Position(3255, 3481, 0)).walk();
        }
        return 300;
    }
}